Nom du projet
=============

Phrase qui décrit le but de ce projet.


Prérequis
---------

- Python 3.5
- Installer MongoDB en local
- NodeJS


Installation
------------

Pour installer les dépendances :

    npm install


Lancement
---------

Pour lancer l'API 

    python main.py

Pour lancer le frontend

    node main.js
